require 'json'
require 'date'
require 'pp'

class RentsPresenter

  def initialize(rents)
    @rents = rents
  end

  def present
    result = {}
    result['rentals'] = []
    @rents.each_with_index do |rent, index|
      actions = []
      actions.push({'who' => 'driver', 'type' => 'debit', 'amount' => rent.total_price })
      actions.push({'who' => 'owner', 'type' => 'credit', 'amount' => rent.commission.owner_price})
      actions.push({'who' => 'insurance', 'type' => 'credit', 'amount' => rent.commission.insurance_fee})
      actions.push({'who' => 'assistance', 'type' => 'credit', 'amount' => rent.commission.assistance_fee})
      actions.push({'who' => 'drivy', 'type' => 'credit', 'amount' => rent.commission.drivy_fee})
      result['rentals'].push({'id' => index + 1, 'options' => rent.options.to_a, 'actions' => actions})
    end
    result.to_json
  end
end

class Car
  attr_reader :car

  def initialize(car)
    @car = car
  end

  def price_per_day
    car.dig('price_per_day')
  end

  def price_per_km
    car.dig('price_per_km')
  end
end

class Commission
  attr_reader :rental
  attr_accessor :commission

  def initialize(rental)
    @rental = rental
    @commission = {}
    compute_commission
  end

  def compute_commission
    owner_price = rental.send('rental_price_per_day_without_options') + rental.distance_price
    price_commission = (owner_price * 0.3).to_i
    commission['owner_price'] = (owner_price * 0.7).to_i
    commission['insurance_fee'] = (price_commission / 2).to_i
    commission['assistance_fee'] = (rental.nbr_days * 100).to_i
    commission['drivy_fee'] = (price_commission / 2) - commission['assistance_fee']
    commission['owner_price'] += (500 * rental.nbr_days) if rental.options.gps?
    commission['owner_price'] += (200 * rental.nbr_days) if rental.options.baby_seat?
    commission['drivy_fee'] += (1000 * rental.nbr_days) if rental.options.additional_insurance?
  end

  def drivy_fee
    commission['drivy_fee']
  end

  def owner_price
    commission['owner_price']
  end

  def insurance_fee
    commission['insurance_fee']
  end

  def assistance_fee
    commission['assistance_fee']
  end
end

class Options
  attr_reader :options

  def initialize(json, rent_id)
    @options = []
    json.each do |option|
      @options.push(option.dig('type')) if option.dig('rental_id') == rent_id
    end
  end

  def to_a
    @options
  end

  def gps?
    @options.include?('gps')
  end

  def baby_seat?
    @options.include?('baby_seat')
  end

  def additional_insurance?
    @options.include?('additional_insurance')
  end
end

class Rental
  attr_reader :car
  attr_reader :options
  attr_reader :rental
  attr_reader :commission

  def initialize(rental, car, options)
    @rental = rental
    @car = Car.new(car)
    @options = Options.new(options, id)
    @commission = Commission.new(self)
  end

  def id
    rental.dig('id')
  end

  def car_id
    rental.dig('car_id')
  end

  def distance
    rental.dig('distance')
  end

  def end_date
    DateTime.parse(rental.dig('end_date'))
  end

  def start_date
    DateTime.parse(rental.dig('start_date'))
  end

  def nbr_days
    (end_date - start_date).to_i + 1
  end

  def day_price
    rental_price_per_day_with_options
  end

  def distance_price
    car.price_per_km * distance
  end

  def total_price
    day_price + distance_price
  end

  private

  def rental_price_per_day_without_options
    final_price = if nbr_days == 1
                    (car.price_per_day * nbr_days).to_i
                  elsif nbr_days <= 4
                    (car.price_per_day + ((car.price_per_day * 0.9) * (nbr_days - 1))).to_i
                  elsif nbr_days <= 10
                    (car.price_per_day + ((car.price_per_day * 0.9) * 3) + ((car.price_per_day * 0.7) * (nbr_days - 4))).to_i
                  else
                    (car.price_per_day + ((car.price_per_day * 0.9) * 3) + ((car.price_per_day * 0.7) * 6) + ((car.price_per_day * 0.5) * (nbr_days - 10))).to_i
                  end
    final_price
  end

  def rental_price_per_day_with_options
    final_price = rental_price_per_day_without_options
    final_price += (500 * nbr_days) if options.gps?
    final_price += (200 * nbr_days) if options.baby_seat?
    final_price += (1000 * nbr_days) if options.additional_insurance?
    final_price
  end

end

class LevelFive
  INPUT = './data/input.json'
  OUTPUT = './data/output.json'

  attr_reader :cars
  attr_reader :rentals
  attr_reader :car

  def initialize
    @file = File.open(INPUT, 'r')
    @json = JSON.parse(@file.read)

    @cars = @json['cars']
    @rentals = @json['rentals']
    perform
  end

  def perform
    rentals = []

    @rentals.each do |rental|
      rental = Rental.new(rental, cars[rental.dig('car_id') - 1], @json['options'])
      rentals.push(rental)
    end
    File.write(OUTPUT, RentsPresenter.new(rentals).present)
  end
end

LevelFive.new
