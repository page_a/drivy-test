require 'json'
require 'date'
require 'pp'

class LevelTwo
  INPUT = './data/input.json'
  OUTPUT = './data/output.json'

  def initialize
    @file = File.open(INPUT, 'r')
    @json = JSON.parse(@file.read)
    perform
  end

  def perform
    solution = { 'rentals' => []}

    cars = @json['cars']
    rentals = @json['rentals']
    i = 0
    rentals.each do |rental|
      car = cars[rental.dig('car_id') - 1]
      end_date = DateTime.parse(rental.dig('end_date'))
      start_date = DateTime.parse(rental.dig('start_date'))
      nbr_days = (end_date - start_date).to_i + 1
      day_price = rental_price_per_day(car.dig('price_per_day'), nbr_days)
      distance_price = car.dig('price_per_km') * rental.dig('distance')
      total_price = day_price + distance_price
      solution['rentals'].push({'id' => i + 1, 'price' => total_price})
      i = i + 1
    end
    File.write(OUTPUT, solution)
  end

  def rental_price_per_day(price_per_day, nbr_day)
    return (price_per_day * nbr_day).to_i if nbr_day == 1
    return (price_per_day + ((price_per_day * 0.9) * (nbr_day - 1))).to_i if nbr_day <= 4
    return (price_per_day + ((price_per_day * 0.9) * 3) + ((price_per_day * 0.7) * (nbr_day - 4))).to_i if nbr_day <= 10
    (price_per_day + ((price_per_day * 0.9) * 3) + ((price_per_day * 0.7) * 6) + ((price_per_day * 0.5) * (nbr_day - 10))).to_i
  end
end

LevelTwo.new
