require 'json'
require 'date'
require 'pp'

class RentalPresenter

  def initialize(data)
    @data = data
  end

  def present
    result = {}
    result['rentals'] = []
    @data.each_with_index do |datum, index|
      actions = []
      actions.push({'who' => 'driver', 'type' => 'debit', 'amount' => datum.dig('price')})
      actions.push({'who' => 'owner', 'type' => 'credit', 'amount' => datum.dig('commission', 'owner_price')})
      actions.push({'who' => 'insurance', 'type' => 'credit', 'amount' => datum.dig('commission', 'insurance_fee')})
      actions.push({'who' => 'assistance', 'type' => 'credit', 'amount' => datum.dig('commission', 'assistance_fee')})
      actions.push({'who' => 'drivy', 'type' => 'credit', 'amount' => datum.dig('commission', 'drivy_fee')})
      result['rentals'].push({'id' => index + 1, 'actions' => actions})
    end
    result.to_json
  end
end

class LevelFour
  INPUT = './data/input.json'
  OUTPUT = './data/output.json'

  def initialize
    @file = File.open(INPUT, 'r')
    @json = JSON.parse(@file.read)
    perform
  end

  def perform
    solution = []

    cars = @json['cars']
    rentals = @json['rentals']
    i = 0
    rentals.each do |rental|
      car = cars[rental.dig('car_id') - 1]
      end_date = DateTime.parse(rental.dig('end_date'))
      start_date = DateTime.parse(rental.dig('start_date'))
      nbr_days = (end_date - start_date).to_i + 1
      day_price = rental_price_per_day(car.dig('price_per_day'), nbr_days)
      distance_price = car.dig('price_per_km') * rental.dig('distance')
      total_price = day_price + distance_price
      commission = compute_commission(total_price, nbr_days)
      solution.push({'id' => i + 1, 'price' => total_price, 'commission' => commission})
      i = i + 1
    end
    File.write(OUTPUT, RentalPresenter.new(solution).present)
  end

  def compute_commission(price, days)
    commission = {}
    price_commission = (price * 0.3).to_i
    commission['owner_price'] = (price * 0.7).to_i
    commission['insurance_fee'] = (price_commission / 2).to_i
    commission['assistance_fee'] = (days * 100).to_i
    commission['drivy_fee'] = (price_commission / 2) - commission['assistance_fee']
    commission
  end

  def rental_price_per_day(price_per_day, nbr_day)
    return (price_per_day * nbr_day).to_i if nbr_day == 1
    return (price_per_day + ((price_per_day * 0.9) * (nbr_day - 1))).to_i if nbr_day <= 4
    return (price_per_day + ((price_per_day * 0.9) * 3) + ((price_per_day * 0.7) * (nbr_day - 4))).to_i if nbr_day <= 10
    (price_per_day + ((price_per_day * 0.9) * 3) + ((price_per_day * 0.7) * 6) + ((price_per_day * 0.5) * (nbr_day - 10))).to_i
  end
end

LevelFour.new
