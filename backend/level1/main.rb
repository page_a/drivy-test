require 'json'
require 'date'
require 'pp'

class LevelOne
  INPUT = './data/input.json'
  OUTPUT = './data/output.json'

  def initialize
    @file = File.open(INPUT, 'r')
    @json = JSON.parse(@file.read)
    perform
  end

  def perform
    solution = { 'rentals' => []}

    cars = @json['cars']
    rentals = @json['rentals']
    i = 0
    rentals.each do |rental|
      car = cars[rental.dig('car_id') - 1]
      end_date = DateTime.parse(rental.dig('end_date'))
      start_date = DateTime.parse(rental.dig('start_date'))
      nbr_days = (end_date.day - start_date.day) + 1
      day_price = nbr_days * car.dig('price_per_day')
      distance_price = car.dig('price_per_km') * rental.dig('distance')
      total_price = day_price + distance_price
      solution['rentals'].push({'id' => i + 1, 'price' => total_price})
      i = i + 1
    end
    File.write(OUTPUT, solution)
  end
end

LevelOne.new
